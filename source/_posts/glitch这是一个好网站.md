---
title: glitch这是一个好网站
cover: https://cdn.jsdelivr.net/gh/xiaoxiaofeiweb/tuku@main/img/20210809191449.png
updated: 2021-08-09 19:06:22
date: 2021-08-09 19:06:22
categories: 技术
tags: 网站
---
# 关于用glitch来搭建博客
## 链接：[https://glitch.com/](https://glitch.com/)
#### 可以用hexo搭建完之后源码上传到GitHub
#### 之后用glitch部署博客
![](https://cdn.jsdelivr.net/gh/xiaoxiaofeiweb/tuku@main/img/20210809190935.png)
![](https://cdn.jsdelivr.net/gh/xiaoxiaofeiweb/tuku@main/img/20210809191051.png)
![](https://cdn.jsdelivr.net/gh/xiaoxiaofeiweb/tuku@main/img/20210809191121.png)
之后填写自己的GitHub仓库地址即可。。
- [静态博客技术]
- 静态博客

