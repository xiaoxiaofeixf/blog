---
title: 白嫖hax的1C450M免费VPS，让您实现白嫖VPS
permalink: 
cover: https://s4.ax1x.com/2021/12/31/T4Hlm4.png
updated: 2021-12-25 22:27:25
date: 2021-12-25 22:27:25
categories: 
tags: 白嫖
---
## 准备工具
TG账号
## 系统配置
1 CPU
450M ram
5G SSD
IPV6
## 申请步骤
1. 打开 [HAX](http://hax.co.id/ "HAX")
2. TG，搜索并打开@HaxTG_bot，并启动
3. 搜索@HaxTG_bot，获取用户ID
[![T45IKJ.md.png](https://s4.ax1x.com/2021/12/31/T45IKJ.md.png)](https://imgtu.com/i/T45IKJ)
4. 在官网中输入用户ID，点击Submit
[![T45X8O.md.png](https://s4.ax1x.com/2021/12/31/T45X8O.md.png)](https://imgtu.com/i/T45X8O)
5. TG，复制机器人@HaxTG_bot发送的注册代码，粘贴至官网，输入密码，点击Submit
[![T4IYM4.png](https://s4.ax1x.com/2021/12/31/T4IYM4.png)](https://imgtu.com/i/T4IYM4)
[![T4IaZR.md.png](https://s4.ax1x.com/2021/12/31/T4IaZR.md.png)](https://imgtu.com/i/T4IaZR)
6. 点击Login按钮登录
[![T4IDJK.png](https://s4.ax1x.com/2021/12/31/T4IDJK.png)](https://imgtu.com/i/T4IDJK)
7. 点击Create one here，设置系统信息，，选择机房及用途（EU1-5是kvm区，EU-OpenVZ是ovz区），同意协议，等待网站配置好您的VPS服务器，大概需要1-3分钟的时间
[![T4IcsH.png](https://s4.ax1x.com/2021/12/31/T4IcsH.png)](https://imgtu.com/i/T4IcsH)
8. 待看到信息时，申请完成
[![T4oCy4.png](https://s4.ax1x.com/2021/12/31/T4oCy4.png)](https://imgtu.com/i/T4oCy4)
## 链接免费服务器
1. 首先选择Pv4 to IPv6
[![T4TMCV.png](https://s4.ax1x.com/2021/12/31/T4TMCV.png)](https://imgtu.com/i/T4TMCV)
2. 之后如图所示操作
[![T4TWPP.png](https://s4.ax1x.com/2021/12/31/T4TWPP.png)](https://imgtu.com/i/T4TWPP)
3. 操作完成后用刚刚写的，如eu1为91.134.238.131加自己添加的端口号
[![T4TzMF.png](https://s4.ax1x.com/2021/12/31/T4TzMF.png)](https://imgtu.com/i/T4TzMF)
4. 输入密码和用户名root，点击链接
[![T47pqJ.png](https://s4.ax1x.com/2021/12/31/T47pqJ.png)](https://imgtu.com/i/T47pqJ)
5. 成功后如图所示
[![T47kPx.png](https://s4.ax1x.com/2021/12/31/T47kPx.png)](https://imgtu.com/i/T47kPx)
## 特别注意
该费vps为站长个人提供，挖矿者必封！！！

##### ps：部分教程借鉴于misakablog