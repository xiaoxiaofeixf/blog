---
title: 记一次c++学习
permalink: 
cover: https://gitee.com/xiaoxiaofeihh/tuku/raw/main/104035-1574908835af5d.jpg
updated: 2022-01-01 14:04:20
date: 2022-01-01 14:04:20
categories: 编程
tags: 编程
---
## C++ Supplementary
### 头文件
```cpp
// MyHeader.h
#ifndef CLIONTEST_MY_HEADER_H
#define CLIONTEST_MY_HEADER_H

#include <iostream>

using namespace std;

extern int x;

void f();

#endif
```
### aux.cpp
```cpp
// aux.cpp
#include "MyHeader.h"

void f() {
    cout << "x is " << x << endl;
}

/*
 * main.o ------------
 * int x;
 * int main();
 *
 * aux.o --------------
 * extern int x;
 * void f(); ---> uses x
 */
```
###main.cpp
```cpp
// main.cpp
#include <iostream>

using namespace std;

int main() {
    cout << "this is out" << endl;
}

/*
 *     stdin    +-----+   stdout
 * ===========->| cpp |===========->
 *              +-----+
 *
 * I/O: In/Out
 */
```